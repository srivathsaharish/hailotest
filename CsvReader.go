package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

// CsvReader is an abstract type to read csv records
// locationsFile is the File object and action is the callback method that will be invoked
// when a csv record is successfully read from the file
type CsvReader interface {
	ReadCsvRecords(locationsFile *os.File, action func(record []string))
}

// FileGetter is the abstract type that is used to open and read files
// filePath is the absolute or the relative path to the file that needs to be read
type FileGetter interface {
	GetFile(filePath string) (*os.File, error)
}

// DefaultCsvReader is the data structure that gives the default implementation for CsvReader
type DefaultCsvReader struct{}

// DefaultFileGetter is the data structure that gives the default implementation for FileGetter
type DefaultFileGetter struct{}

// GetFile Implementation for DefaultFileGetter
// The permission with which the file is opened is os.O_RDWR|os.O_CREATE, os.ModePerm
func (fileGetter DefaultFileGetter) GetFile(filePath string) (*os.File, error) {
	return os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, os.ModePerm)
}

// ReadCsvRecords implementation for DefaultCsvReader
// This internally uses encoding/csv reader
// On successful fetching of each csv record the callback action method is invoked
func (csvReader DefaultCsvReader) ReadCsvRecords(locationsFile *os.File, action func(record []string)) {
	file := locationsFile
	defer file.Close()
	reader := csv.NewReader(file)
	reader.Comma = ','
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error:", err)
			continue
		}

		action(record)
	}
}
