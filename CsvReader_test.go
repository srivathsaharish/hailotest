package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetFile(t *testing.T) {
	fileGetter := new(DefaultFileGetter)

	_, err := fileGetter.GetFile("")
	assert.NotNil(t, err)

	locationsFile, err := fileGetter.GetFile("5937.csv")
	assert.Nil(t, err)
	assert.NotNil(t, locationsFile)
}
