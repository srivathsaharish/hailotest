package main

import "sync"

// Dispatcher is the abstract type that publish the ocation information of the LocationInfo objects from a datasource
type Dispatcher interface {
	Dispatch(locationsChannel chan []string, signal *sync.WaitGroup, action func(record []string))
}

// DefaultDispatcher is the default implementation of Dispatcher interface
type DefaultDispatcher struct{}

// Dispatch implementation for DefaultDispatcher
// For every LocationInfo received in locationsChannel, action will be invoked
func (dispatcher DefaultDispatcher) Dispatch(locationsChannel chan []string, signal *sync.WaitGroup, action func(record []string)) {
	defer signal.Done()
	for record := range locationsChannel {
		action(record)
	}

	close(locationsChannel)
}
