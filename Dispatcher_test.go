package main

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

//TODO Add more test cases
func TestDispatch(t *testing.T) {
	locationsChannel := make(chan []string)
	var signal sync.WaitGroup
	signal.Add(1)
	dispatcher := new(DefaultDispatcher)
	counter := 0
	go dispatcher.Dispatch(locationsChannel, &signal, func(record []string) {
		counter++
	})
	go func() {
		signal.Wait()
		assert.Equal(t, counter, 1)
	}()
}
