package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Driver is the data structure to represent a robot driver
type Driver struct {
	ID           string
	LastLocation *LocationInfo
}

// DriverManager is the abstract type that defines all the methods that can be invoked on a Manager
type DriverManager interface {
	GetDriver() *Driver
	RecieveLocation(driver *Driver, locationChannel chan *LocationInfo, signal *sync.WaitGroup, isNearBy func(location *LocationInfo) bool)
	AssignID(driver *Driver, id string)
	SendTrafficCondition(trafficInfo *TrafficInfo, trafficInfoChannel chan *TrafficInfo)
	ShutDown(driver *Driver)
	Move(driver *Driver, location *LocationInfo)
}

// DefaultDriverManager is the type that implements the DriverManager interface
type DefaultDriverManager struct{}

// GetDriver implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) GetDriver() *Driver {
	return &Driver{}
}

// RecieveLocation implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) RecieveLocation(driver *Driver, locationChannel chan *LocationInfo,
	signal *sync.WaitGroup, trafficInfoChannel chan *TrafficInfo, isNearBy func(location *LocationInfo) bool) {
	defer signal.Done()
	for location := range locationChannel {
		driverManager.Move(driver, location)
		if isNearBy(location) {
			trafficInfo := TrafficInfo{}
			trafficInfo.Condition = TrafficCondition(rand.Intn(3) + 1)
			trafficInfo.DriverID = driver.ID
			trafficInfo.Speed = rand.Intn(80)
			trafficInfo.Time = time.Now()
			go driverManager.SendTrafficCondition(&trafficInfo, trafficInfoChannel)
		}
	}
}

// AssignID implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) AssignID(driver *Driver, id string) {
	driver.ID = id
}

// SendTrafficCondition implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) SendTrafficCondition(trafficInfo *TrafficInfo, trafficInfoChannel chan *TrafficInfo) {
	fmt.Printf("Sent-%+v\n", trafficInfo)
	trafficInfoChannel <- trafficInfo
}

// ShutDown implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) ShutDown(driver *Driver) {
}

// Move implementation for DefaultDriverManager
func (driverManager DefaultDriverManager) Move(driver *Driver, location *LocationInfo) {
	driver.LastLocation = location
}
