package main

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

//TODO Add more test cases
func TestGetDriver(t *testing.T) {
	driverManager := new(DefaultDriverManager)
	driver := driverManager.GetDriver()
	assert.NotNil(t, driver)
}

func TestAssignID(t *testing.T) {
	driverManager := new(DefaultDriverManager)
	driver := driverManager.GetDriver()
	driverManager.AssignID(driver, "1")
	assert.Equal(t, driver.ID, "1")
}

func TestMove(t *testing.T) {
	driverManager := new(DefaultDriverManager)
	driver := driverManager.GetDriver()
	assert.Empty(t, driver.LastLocation)
	locationInfo := LocationInfo{}
	locationInfo.DriverID = "DriverID"
	locationInfo.Latitude = "Latitude"
	locationInfo.Longitude = "Longitude"
	locationInfo.Time = time.Now()
	driverManager.Move(driver, &locationInfo)
	assert.Equal(t, locationInfo.DriverID, driver.LastLocation.DriverID)
	assert.Equal(t, locationInfo.Latitude, driver.LastLocation.Latitude)
	assert.Equal(t, locationInfo.Longitude, driver.LastLocation.Longitude)
	assert.Equal(t, locationInfo.Time, driver.LastLocation.Time)
}

func TestRecieveLocation(t *testing.T) {
	driverManager := new(DefaultDriverManager)
	driver := driverManager.GetDriver()
	driverManager.AssignID(driver, "1")
	locationChannel := make(chan *LocationInfo)
	trafficInfoChannel := make(chan *TrafficInfo)
	var signal sync.WaitGroup
	signal.Add(1)
	counter := 0
	locationInfo := LocationInfo{}
	locationInfo.DriverID = "DriverID"
	locationInfo.Latitude = "Latitude"
	locationInfo.Longitude = "Longitude"
	locationInfo.Time = time.Now()
	go func() {
		locationChannel <- &locationInfo
	}()
	go driverManager.RecieveLocation(driver, locationChannel, &signal, trafficInfoChannel, func(location *LocationInfo) bool {
		counter++
		return true
	})
	go func() {
		trafficInfo := <-trafficInfoChannel
		assert.Equal(t, locationInfo.DriverID, trafficInfo.DriverID)
		assert.Equal(t, counter, 1)
	}()
}
