package main

import "time"

// Locations is the slice that reprsents a collection of LocationInfo pointers
type Locations []*LocationInfo

// LocationInfo represents the object that is read from the data source that posts the
// x & y co-ordinates of a driver at a given time
type LocationInfo struct {
	DriverID  string
	Latitude  string
	Longitude string
	Time      time.Time
}
