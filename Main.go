package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	tubeFile := "Data\\tube.csv"

	fileGetter := new(DefaultFileGetter)
	csvReader := new(DefaultCsvReader)
	dispatcher := new(DefaultDispatcher)
	driverManager := new(DefaultDriverManager)
	distanceChecker := new(BruteForceTubeDriverDistanceChecker)

	drivers := []*Driver{}

	driver1 := driverManager.GetDriver()
	driverManager.AssignID(driver1, "5937")
	drivers = append(drivers, driver1)

	driver2 := driverManager.GetDriver()
	driverManager.AssignID(driver2, "6043")
	drivers = append(drivers, driver2)

	driverIDRecieverChannelMap := map[string]chan *LocationInfo{}

	csvReaderSignals := []sync.WaitGroup{}
	recieverSignals := []sync.WaitGroup{}

	var dispatcherSignal sync.WaitGroup
	var tubeCsvReaderSignal sync.WaitGroup

	tubeCsvReaderSignal.Add(1)
	dispatcherSignal.Add(1)

	tubeStations := []*TubeInfo{}
	go readCsvRecords(tubeFile, fileGetter, csvReader, &tubeCsvReaderSignal, func(record []string) {
		tubeInfo := TubeInfo{}
		tubeInfo.Name = record[0]
		tubeInfo.Latitude = record[1]
		tubeInfo.Longitude = record[2]
		tubeStations = append(tubeStations, &tubeInfo)
	})

	tubeCsvReaderSignal.Wait()

	locationChannel := make(chan []string)
	trafficInfoChannel := make(chan *TrafficInfo)

	go dispatcher.Dispatch(locationChannel, &dispatcherSignal, func(record []string) {
		locationInfo := LocationInfo{}
		timeFormat := "2006-01-02 15:04:05"

		locationInfo.DriverID = record[0]
		locationInfo.Latitude = record[1]
		locationInfo.Longitude = record[2]
		locationTime, timeError := time.Parse(timeFormat, record[3])
		if timeError != nil {
			return
		}

		locationInfo.Time = locationTime
		val, ok := driverIDRecieverChannelMap[locationInfo.DriverID]
		if ok {
			go func() {
				val <- &locationInfo
			}()
		} else {
			receiverChannel := make(chan *LocationInfo, 10)
			driverIDRecieverChannelMap[locationInfo.DriverID] = receiverChannel
		}

		//fmt.Printf("%+v\n", locationInfo)
	})

	for _, driver := range drivers {
		var csvReaderSignal sync.WaitGroup
		var recieverSignal sync.WaitGroup
		csvReaderSignal.Add(1)
		recieverSignal.Add(1)

		csvReaderSignals = append(csvReaderSignals, csvReaderSignal)
		recieverSignals = append(recieverSignals, recieverSignal)

		receiverChannel := make(chan *LocationInfo, 10)
		driverIDRecieverChannelMap[driver.ID] = receiverChannel

		_, ok := driverIDRecieverChannelMap[driver.ID]
		if !ok {
			receiverChannel := make(chan *LocationInfo, 10)
			driverIDRecieverChannelMap[driver.ID] = receiverChannel
		}

		go readCsvRecords("Data\\"+driver.ID+".csv", fileGetter, csvReader, &csvReaderSignal, func(record []string) {
			locationChannel <- record
		})

		go driverManager.RecieveLocation(driver, receiverChannel, &recieverSignal, trafficInfoChannel,
			func(location *LocationInfo) bool {
				return distanceChecker.IsDriverNearByToTube(tubeStations, driver, 1)
			})
	}

	go func() {
		for _, csvReaderSignal := range csvReaderSignals {
			csvReaderSignal.Wait()
		}
		fmt.Println("CsvReading Done")
	}()

	go func() {
		dispatcherSignal.Wait()
		fmt.Println("Dispatch Done")
	}()

	go func() {
		for _, recieverSignal := range recieverSignals {
			recieverSignal.Wait()
		}
		fmt.Println("Processing Done")
	}()

	var input string
	fmt.Scanln(&input)
}

func readCsvRecords(filePath string, fileGetter FileGetter, reader CsvReader,
	wg *sync.WaitGroup, action func(record []string)) {
	defer wg.Done()
	locationsFile, err := fileGetter.GetFile(filePath)
	if err != nil {
		fmt.Println("Error in reading csv file")
		panic(err)
	}

	reader.ReadCsvRecords(locationsFile, action)
}
