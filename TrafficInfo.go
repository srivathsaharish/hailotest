package main

import "time"

// TrafficCondition is the type that represents the intensity of RoadTraffic at a given time
type TrafficCondition int

// This represents the intensity of the traffic on road at given time with the lowest value being the most intense
const (
	HEAVY TrafficCondition = 1 << iota
	MODERATE
	LIGHT
)

// TrafficInfo is the object that is published by the robots
type TrafficInfo struct {
	DriverID  string
	Time      time.Time
	Speed     int
	Condition TrafficCondition
}
