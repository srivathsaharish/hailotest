package main

// TubeInfo represents a tube station
type TubeInfo struct {
	Name      string
	Latitude  string
	Longitude string
}

// TubeDriverDistanceChecker is the abtract type to check if a driver is near by to a tube station
type TubeDriverDistanceChecker interface {
	IsDriverNearByToTube(tubeInfo []*TubeInfo, driver *Driver, nearbyDistanceLimit int) bool
}

// BruteForceTubeDriverDistanceChecker is the concrete implementation of TubeDriverDistanceChecker
// It does not try to return accurate results rather it tries to return an okay result
type BruteForceTubeDriverDistanceChecker struct{}

// IsDriverNearByToTube implementation for BruteForceTubeDriverDistanceChecker
func (BruteForceTubeDriverDistanceChecker) IsDriverNearByToTube(tubeInfo []*TubeInfo, driver *Driver, nearbyDistanceLimit int) bool {
	//TODO Implemet correct logic
	return true
}
